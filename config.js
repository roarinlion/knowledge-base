const config = {
  gatsby: {
    pathPrefix: '/',
    siteUrl: 'https://learn.dealerclick.com',
    gaTrackingId: null,
    trailingSlash: false,
  },
  header: {
    logo: 'https://dealerclick.com/wp-content/uploads/2020/11/dclogo.png',
    logoLink: 'https://learn.dealerclick.com',
    title: '',
    links: [{ text: '', link: '' }],
    search: {
      enabled: false,
      indexName: '',
      algoliaAppId: process.env.GATSBY_ALGOLIA_APP_ID,
      algoliaSearchKey: process.env.GATSBY_ALGOLIA_SEARCH_KEY,
      algoliaAdminKey: process.env.ALGOLIA_ADMIN_KEY,
    },
  },
  sidebar: {
    forcedNavOrder: [
      '/introduction', // add trailing slash if enabled above
      '/login',
      '/dashboard',
      '/inventory',
      '/contacts',
      '/deals',
      '/reports',
      '/vendors',
      '/payments',
      '/settings',
    ],
    collapsedNav: [
      '/login',
      '/dashboard',
      '/contacts',
      '/deals',
      '/inventory',
      '/settings',
      '/reports'
    ],
    links: [{ text: 'Help', link: 'https://autocomp.freshdesk.com' }],
    frontline: false,
    ignoreIndex: true,
    title: 'Table of Contents',
  },
  siteMetadata: {
    title: '',
    description: 'Documentation for DealerClick 2.0',
    ogImage: null,
    docsLocation: '',
    favicon: '',
  },
  pwa: {
    enabled: false, // disabling this will also remove the existing service worker.
    manifest: {
      name: 'DealerClick Learn',
      short_name: 'DCLearn',
      start_url: '/',
      background_color: '#6b37bf',
      theme_color: '#6b37bf',
      display: 'standalone',
      crossOrigin: 'use-credentials',
      icons: [
        {
          src: 'src/pwa-512.png',
          sizes: `512x512`,
          type: `image/png`,
        },
      ],
    },
  },
};

module.exports = config;
