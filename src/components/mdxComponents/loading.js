import * as React from 'react';

const LoadingProvider = ({ ...props }) => {
  return <div>loading...</div>;
};

export default LoadingProvider;
