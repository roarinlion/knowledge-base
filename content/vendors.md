---
title: "Vendors"
metaTitle: "DealerClick Learn"
metaDescription: "Learn DealerClick 2.0!"
---

For all vendor types, their information can be entered under the Vendors module.

All tabs in the Vendor module have the same format, they have the main page where the user can see a list of vendors and the ability to edit or delete them. Near the top of all the entries, you will see the ability to add a new vendor.

Below is an example of the Reconditioning tab:

![Vendors Reconditioning](./vendors/vendorsReconditioningTab.png)


