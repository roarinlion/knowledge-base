---
title: "Payments"
metaTitle: "DealerClick Learn"
metaDescription: "Learn DealerClick 2.0!"
---
The user may be able to process payments throuh the Payment Terminal. To use the payment terminal, the user must first be enrolled with [Digitzs](https://digitzs.com/). Digitzs is a third party credit card processing service, please contact them for details.

![Payment Terminal](./payments/paymentTerminal.png)


