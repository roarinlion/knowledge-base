---
title: "Settings"
metaTitle: "DealerClick Learn"
metaDescription: "Learn DealerClick 2.0!"
---


Under the settings module you can select different defaults for the entire program.

## Profile

This tab will display the user's information and their comission defaults.

![Profile Tab](./settings/settingsProfileTab.png)

## Company Information

This tab will display company information such as address, phone numbers, and email addresses. 

![Company Information Tab](./settings/settingsCompanyInformation.png)

## Employees

The Employees tab is where you may enter or edit all the employees within the system if you have administrator privileges.

![Employees Tab](./settings/settingsEmployeeTab.png)

When adding a new employee, a window will open up allowing you to enter all the information of an employee and also allow you to enter images of the employee if desired.

![Employees Form Information](./settings/settingsEmployeeFormInformation.png)

Near the bottom of the form you will the ability to enter the commission defaults for the employee and their role and privileges.

![Employees Form Comission And Privileges](./settings/settingsEmployeeFormComissionsAndPrivileges.png)

## Tax Defaults

Tax default rates and settings are adjusted here. The user has the option to of setting the rates to the default tax rate, none, or a specific rate. When selecting a specific rate, a new field will appear to enter the value for the specific default that the user wants changed. 

![Employees Form Comission And Privileges](./settings/settingsEmployeeFormComissionsAndPrivileges.png)

## Profit Settings

Under the Profit tab, the user can decide whether certain fees will have front end, backend, or no profit.

![Profit](./settings/settingsProfitSettingsTab.png)

## Commissions

The user can decide what fees are commissionable in this tab, to include if they are commissionable in the Front or Back or both (SP & FI).

![Comissions](./settings/settingsProfitSettingsTab.png)

## Sign Ups

Here relevant links to third party integrations are listed so that the user may sign up with them.

![Sign Up Tab](./settings/settingsSignUpsTab.png)

Below is an example with the credit card processing company Digitzs:

![Sign Up Digitsz](./settings/settingsDigitzsSignUpForm.png)

## The Deal Template

Under the settings you can select defaults for the entirety of the program. This module consists of 5 areas where relevant background information of a Deal is entered.

![Deal Template](./settings/settingsDealTemplate.png)

Below is an example of opening the Service Contract window to enter information for a service contract company:

![Deal Template Service Contract](./settings/settingsDealTemplatesServiceContractsForm.png)