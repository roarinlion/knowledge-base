---
title: "Contacts"
metaTitle: "DealerClick Learn"
metaDescription: "Learn DealerClick 2.0!"
---

The Contacts module is where all the Customer and Lead related items will be maintained. Here the user will:

[Add Contacts](./contacts/contactsAddingLeads)

[View Contacts](./contacts/contactsViewingLeads)

[View Lead Reports](./contacts/contactsLeadReports)
