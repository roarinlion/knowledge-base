---
title: "Inventory"
metaTitle: "DealerClick Learn"
metaDescription: "Learn DealerClick 2.0!"
---

The Inventory module is where all the vehicle related items will be maintained. Here the user will:

[Add Vehicles](./inventory/inventoryAddingVehicle)

[Advertise Vehicles](./inventory/inventoryAdvertising)

[Appraise Vehicles](./inventory/inventoryAppraising)

[View Reports for Vehicles](./inventory/inventoryReports)

[View and Edit Vehicles](./inventory/inventoryViewAndEdit)
