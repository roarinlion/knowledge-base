---
title: "Login"
metaTitle: "DealerClick Learn"
metaDescription: "Learn DealerClick 2.0!"
---

The Login Screen appears as soon as you enter into the [application](delaerclick.app)

![Login Screen](/login/Login.PNG)


