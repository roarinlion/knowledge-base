---
title: "Deals"
metaTitle: "DealerClick Learn"
metaDescription: "Learn DealerClick 2.0!"
---

The Deal module is where deal related tasks will be accomplished Here the user will:

[Create a New Deal](./deals/dealsNewDeal)

[View and Edit Deals](./deals/dealsViewDeals)
